# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :demogame, Demogame.Cache,
  gc_interval: 86_400, # 24 hrs
  otp_app: :demogame,
  adpater: Nebulex.Adapters.Local


config :demogame,
  ecto_repos: [Demogame.Repo]

# Configures the endpoint
config :demogame, DemogameWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "bN9WiHWG7hH/kML7Q0RPEEz/8JFbCg9Y+Vy8UdWe19mZmpdsMY4e5UxuvaGq2xXJ",
  render_errors: [view: DemogameWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Demogame.PubSub,
  live_view: [signing_salt: "ZIvfctWV"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
