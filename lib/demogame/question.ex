defmodule Demogame.Question do
  use Ecto.Schema
  import Ecto.Changeset

  schema "questions" do
    field :category_id, :integer
    field :points, :integer
    field :text, :string

    timestamps()
  end

  @doc false
  def changeset(question, attrs) do
    question
    |> cast(attrs, [:category_id, :points, :text])
    |> validate_required([:category_id, :points, :text])
  end
end
