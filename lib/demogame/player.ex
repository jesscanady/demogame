defmodule Demogame.Player do
  use Ecto.Schema
  import Ecto.Changeset

  schema "players" do
    field :name, :string
    belongs_to :game, Demogame.Game

    timestamps()
  end

  @doc false
  def changeset(player, attrs) do
    player
    |> cast(attrs, [:game_id, :name])
    |> validate_required([:game_id, :name])
  end
end
