defmodule Demogame.Repo do
  use Ecto.Repo,
    otp_app: :demogame,
    adapter: Ecto.Adapters.Postgres
end
