defmodule DemogameWeb.JeopardyLive.SelectCategory do
  use DemogameWeb, :live_view
  alias Demogame.Jeopardy

  @impl true
  def mount(params, _session, socket) do
    player = Jeopardy.find_player(params["id"])
    game   = Jeopardy.find_game(player.game_id)

    if connected?(socket) do
      Phoenix.PubSub.subscribe(Demogame.PubSub, "game:#{game.id}")
      Phoenix.PubSub.subscribe(Demogame.PubSub, "game:#{game.id}:#{player.id}")
    end

    if game.current_question_id do
      {:ok, push_redirect(socket, to: Routes.answer_path(socket, DemogameWeb.JeopardyLive.Answer, game.current_question_id, player.id))}
    else
      {:ok, socket
      |> assign(game: game)
      |> assign(current_player: player)
      |> assign(chosen_category: nil)}
    end
  end

  @impl true
  def handle_event("category_select", %{"category" => category_id}, socket) do
    {:noreply, socket
    |> assign(chosen_category: category_id)}
  end

  @impl true
  def handle_event("points_select", %{"points" => _points}, socket) do
    # question = Jeopardy.find_question(socket.assigns.chosen_category, points)
    question = %{id: 1}
    %{game: game, current_player: player} = socket.assigns

    Jeopardy.select_question_for_game(game, question)

    broadcast(game.id, {:question_selected, question.id})
    {:noreply, socket
    |> push_redirect(to: Routes.answer_path(socket, DemogameWeb.JeopardyLive.Answer, question.id, player.id))}
  end

  @impl true
  def render(%{chosen_category: category} = assigns) when not is_nil(category) do
    ~L"""
      <p class="text-2xl text-gray-100 center">Please select a dollar value:</p>

      <button phx-click="points_select"
              phx-value-points="100"
              id="amount-100"
              class="block mt-2 bg-orange-500 hover:bg-orange-400 text-white text-2xl font-bold py-2 px-4 border-b-4 border-orange-700 hover:border-orange-500 rounded"> $100
      </button>

      <button phx-click="points_select"
              phx-value-points="200"
              id="amount-200"
              class="block mt-2 bg-orange-500 hover:bg-orange-400 text-white text-2xl font-bold py-2 px-4 border-b-4 border-orange-700 hover:border-orange-500 rounded">
              $200
      </button>

      <button phx-click="points_select"
              phx-value-points="300"
              id="amount-300"
              class="block mt-2 bg-orange-500 hover:bg-orange-400 text-white text-2xl font-bold py-2 px-4 border-b-4 border-orange-700 hover:border-orange-500 rounded">
              $300
      </button>

      <button phx-click="points_select"
              phx-value-points="500"
              id="amount-500"
              class="block mt-2 bg-orange-500 hover:bg-orange-400 text-white text-2xl font-bold py-2 px-4 border-b-4 border-orange-700 hover:border-orange-500 rounded">
              $500
      </button>
    """
  end

  def render(assigns) do
    ~L"""
      <p class="text-2xl text-gray-100 center">Please select a category:</p>

      <button phx-click="category_select"
              phx-value-category="1"
              id="category-1"
              class="block mt-2 bg-orange-500 hover:bg-orange-400 text-white text-2xl font-bold py-2 px-4 border-b-4 border-orange-700 hover:border-orange-500 rounded">
        Category 1
      </button>

      <button phx-click="category_select"
              phx-value-category="2"
              id="category-2"
              class="block mt-2 bg-orange-500 hover:bg-orange-400 text-white text-2xl font-bold py-2 px-4 border-b-4 border-orange-700 hover:border-orange-500 rounded">
        Category 2
      </button>

      <button phx-click="category_select"
              phx-value-category="3"
              id="category-3"
              class="block mt-2 bg-orange-500 hover:bg-orange-400 text-white text-2xl font-bold py-2 px-4 border-b-4 border-orange-700 hover:border-orange-500 rounded">
        Category 3
      </button>

      <button phx-click="category_select"
              phx-value-category="4"
              id="category-4"
              class="block mt-2 bg-orange-500 hover:bg-orange-400 text-white text-2xl font-bold py-2 px-4 border-b-4 border-orange-700 hover:border-orange-500 rounded">
        Category 4
      </button>

      <button phx-click="category_select"
              phx-value-category="5"
              id="category-5"
              class="block mt-2 bg-orange-500 hover:bg-orange-400 text-white text-2xl font-bold py-2 px-4 border-b-4 border-orange-700 hover:border-orange-500 rounded">
        Category 5
      </button>
    """
  end

  defp broadcast(game_id, msg) do
    Phoenix.PubSub.broadcast(Demogame.PubSub, "game:#{game_id}", msg)
  end
end
