defmodule DemogameWeb.JeopardyLive.Board do
  use DemogameWeb, :live_view
  alias Demogame.Jeopardy
  alias Demogame.Player

  @impl true
  def mount(params, _session, socket) do
    if connected?(socket) do
      Phoenix.PubSub.subscribe(Demogame.PubSub, "game:#{params["id"]}")
    end

    game = Jeopardy.find_game(params["id"])

    {:ok,
      socket
      |> assign(game: game)
      |> assign_controlling_player}
  end

  @impl true
  def handle_info({:question_selected, question_id}, socket) do
    {:noreply,
    push_redirect(socket, to: Routes.question_path(socket, DemogameWeb.JeopardyLive.Question, socket.assigns.game.id, question_id))}
  end

  @impl true
  def handle_info(_msg, socket) do
    {:noreply, socket}
  end

  defp broadcast(game_id, msg) do
    Phoenix.PubSub.broadcast(Demogame.PubSub, "game:#{game_id}", msg)
  end

  defp broadcast(game_id, player_id, msg) do
    Phoenix.PubSub.broadcast(Demogame.PubSub, "game:#{game_id}:#{player_id}", msg)
  end

  defp assign_controlling_player(socket) do
    game = socket.assigns.game

    controlling_player = Jeopardy.get_or_assign_controlling_player(game)
    broadcast(socket.assigns.game.id, controlling_player.id, {:select_category, controlling_player.id})
    assign(socket, controlling_player_id: controlling_player.id)
  end
end
