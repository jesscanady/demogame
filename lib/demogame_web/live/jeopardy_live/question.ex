defmodule DemogameWeb.JeopardyLive.Question do
  use DemogameWeb, :live_view
  alias Demogame.{Jeopardy, Player}

  @impl true
  def mount(params, _session, socket) do
    if connected?(socket) do
      Phoenix.PubSub.subscribe(Demogame.PubSub, "game:#{params["game_id"]}")
    end

    game = Jeopardy.find_game(params["game_id"])
    # question = Jeopardy.find_question(params["question_id"])
    question = %{id: 1, text: "Which American Revolution-era hero can be said to be the Cincinnatus of his time?"}

    {:ok, socket
    |> assign(game: game)
    |> assign(question: question)
    |> assign(correct: nil)
    |> assign(buzzed_in_player: nil)}
  end

  @impl true
  def handle_info({:player_buzzed_in, player}, socket) do
    {:noreply, assign(socket, buzzed_in_player: player, correct: nil)}
  end

  @impl true
  def handle_info({:question_response, correct}, socket) do
    {:noreply, assign(socket, correct: correct)}
  end

  @impl true
  def handle_info({:buzzer_timeout, _player}, socket) do
    {:noreply, assign(socket, buzzed_in_player: nil)}
  end

  @impl true
  def handle_info(_msg, socket) do
    {:noreply, socket}
  end

  def playerbar_player_classes(_player, nil, _correct) do
    "bg-gray-400"
  end

  def playerbar_player_classes(%Player{id: id1}, %Player{id: id2}, _correct) when id1 != id2 do
    "bg-gray-400"
  end

  def playerbar_player_classes(%Player{id: id1}, %Player{id: id2}, nil) when id1 == id2 do
    "bg-orange-400"
  end

  def playerbar_player_classes(%Player{id: id1}, %Player{id: id2}, true) when id1 == id2 do
    "bg-green-400"
  end

  def playerbar_player_classes(%Player{id: id1}, %Player{id: id2}, false) when id1 == id2 do
    "bg-red-400"
  end
end
