defmodule DemogameWeb.JeopardyLive.Title do
  use DemogameWeb, :live_view
  alias Demogame.Jeopardy

  @impl true
  @spec mount(any, any, any) :: {:ok, any}
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  @spec handle_event(<<_::112>>, any, Phoenix.LiveView.Socket.t()) ::
          {:noreply, Phoenix.LiveView.Socket.t()}
  def handle_event("start_new_game", _params, socket) do
    game = Jeopardy.start_new_game()
    {:noreply,
      push_redirect(socket,
        to: Routes.lobby_path(socket, DemogameWeb.JeopardyLive.Lobby, game.id))}
  end
end
