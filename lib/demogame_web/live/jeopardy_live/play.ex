defmodule DemogameWeb.JeopardyLive.Play do
  use DemogameWeb, :live_view
  alias Demogame.Jeopardy

  @impl true
  def mount(params, _session, socket) do
    player = Jeopardy.find_player(params["id"])
    game   = Jeopardy.find_game(player.game_id)

    if connected?(socket) do
      Phoenix.PubSub.subscribe(Demogame.PubSub, "game:#{game.id}")
      Phoenix.PubSub.subscribe(Demogame.PubSub, "game:#{game.id}:#{player.id}")
    end

    if player.id == game.controlling_player_id do
      {:ok, push_redirect(socket, to: Routes.select_path(socket, DemogameWeb.JeopardyLive.SelectCategory, player.id))}
    else
      {:ok, socket
      |> assign(game: game)
      |> assign(current_player: player)}
    end
  end

  @impl true
  def handle_info({:select_category, player_id}, socket) do
    {:noreply,
     push_redirect(socket, to: Routes.select_path(socket, DemogameWeb.JeopardyLive.SelectCategory, player_id))}
  end

  @impl true
  def handle_info({:question_selected, question_id}, socket) do
    {:noreply,
     push_redirect(socket, to: Routes.answer_path(socket, DemogameWeb.JeopardyLive.Answer, question_id, socket.assigns.current_player.id))}
  end

  @impl true
  def handle_info(_msg, socket) do
    {:noreply, socket}
  end
end
