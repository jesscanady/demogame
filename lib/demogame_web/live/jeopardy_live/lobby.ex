defmodule DemogameWeb.JeopardyLive.Lobby do
  use DemogameWeb, :live_view
  alias Demogame.Jeopardy

  @impl true
  def mount(params, _session, socket) do
    if connected?(socket) do
      Phoenix.PubSub.subscribe(Demogame.PubSub, "game:#{params["id"]}")
    end

    game = Jeopardy.find_game(params["id"])
    {:ok, socket
    |> assign(game: game)
    |> assign(players: game.players)}
  end

  @impl true
  def handle_info(:player_join, socket) do
    {:noreply,
     socket
     |> assign(players: Jeopardy.players_for_game(socket.assigns.game.id))}
  end

  @impl true
  def handle_info(:start_button_pressed, socket) do
    game = socket.assigns.game
           |> Jeopardy.update_game_status("running")

    {:noreply,
      socket
      |> push_redirect(to: Routes.board_path(socket, DemogameWeb.JeopardyLive.Board, game.id))}
  end

  @impl true
  def handle_info(_msg, socket) do
    {:noreply, socket}
  end
end
