defmodule DemogameWeb.JeopardyLive.Answer do
  use DemogameWeb, :live_view
  alias Demogame.Jeopardy

  @impl true
  def mount(params, _session, socket) do
    player = Jeopardy.find_player(params["player_id"])
    # question = Jeopardy.find_question(params["question_id"])
    question = %{id: 1, game: player.game}

    if connected?(socket) do
      Phoenix.PubSub.subscribe(Demogame.PubSub, "game:#{player.game.id}")
    end

    {:ok,
     socket
     |> assign(game: player.game)
     |> assign(current_player: player)
     |> assign(question: question)
     |> assign(state: :ready)}
  end

  @impl true
  def render(%{state: :correct} = assigns) do
    ~L"""
    <p id="answers" class="text-2xl text-gray-100 center">Question answered correctly.</p>
    """
  end

  def render(%{state: :buzzed_in} = assigns) do
    ~L"""
      <p id="answers" class="text-2xl text-gray-100 center">Please select a answer:</p>

      <button phx-click="answer_select"
              phx-value-answer="1"
              id="answer_1"
              class="block mt-2 bg-orange-500 hover:bg-orange-400 text-white text-2xl font-bold py-2 px-4 border-b-4 border-orange-700 hover:border-orange-500 rounded">
        Answer 1
      </button>

      <button phx-click="answer_select"
              phx-value-answer="2"
              id="answer_2"
              class="block mt-2 bg-orange-500 hover:bg-orange-400 text-white text-2xl font-bold py-2 px-4 border-b-4 border-orange-700 hover:border-orange-500 rounded">
        Answer 2
      </button>

      <button phx-click="answer_select"
              phx-value-answer="3"
              id="answer_3"
              class="block mt-2 bg-orange-500 hover:bg-orange-400 text-white text-2xl font-bold py-2 px-4 border-b-4 border-orange-700 hover:border-orange-500 rounded">
        Answer 3
      </button>

      <button phx-click="answer_select"
              phx-value-answer="4"
              id="answer_4"
              class="block mt-2 bg-orange-500 hover:bg-orange-400 text-white text-2xl font-bold py-2 px-4 border-b-4 border-orange-700 hover:border-orange-500 rounded">
        Answer 4
      </button>

      <button phx-click="answer_select"
              phx-value-answer="5"
              id="answer_5"
              class="block mt-2 bg-orange-500 hover:bg-orange-400 text-white text-2xl font-bold py-2 px-4 border-b-4 border-orange-700 hover:border-orange-500 rounded">
        Answer 5
      </button>
    """
  end

  @impl true
  def render(%{state: :locked_out} = assigns) do
    ~L"""
    <div phx-click="buzz" id="buzzer" class="h-screen w-full bg-orange-400 flex justify-center items-center text-center text-5xl font-extrabold">
    TRY AGAIN NEXT TIME :(
    </div>
    """
  end

  @impl true
  def render(%{state: :waiting} = assigns) do
    ~L"""
    <div phx-click="buzz" id="buzzer" class="h-screen w-full bg-orange-400 flex justify-center items-center text-center text-5xl font-extrabold">
    PLEASE WAIT!
    </div>
    """
  end

  @impl true
  def render(assigns) do
    ~L"""
    <div phx-click="buzz" id="buzzer" class="h-screen w-full bg-orange-400 flex justify-center items-center text-center text-5xl font-extrabold">
      TAP TO BUZZ IN
    </div>
    """
  end

  @impl true
  def handle_event("buzz", _params, %{assigns: %{state: :ready}} = socket) do
    broadcast(socket.assigns.game.id, {:player_buzzed_in, socket.assigns.current_player})
    # TODO: What do we do if a player refreshes, which will cause this liveview to go away?
    Process.send_after(self(), :no_answer, 6_000)
    {:noreply, socket |> assign(state: :buzzed_in)}
  end

  @impl true
  def handle_event("buzz", _params, socket) do
    {:noreply, socket}
  end

  @impl true
  def handle_event("answer_select", %{"answer" => answer_id}, socket) do
    %{game: game, question: question} = socket.assigns

    correct = Jeopardy.evaluate_answer(question, answer_id)
    if correct do
      broadcast(game.id, {:question_response, correct})
      Process.send_after(self(), :question_correct, 5_000)
      {:noreply, socket |> assign(state: :correct)}
    else
      broadcast(game.id, {:question_response, correct})
      {:noreply, socket |> assign(state: :locked_out)}
    end
  end

  @impl true
  def handle_info(:question_correct, socket) do
    broadcast(socket.assigns.game.id, :select_category)
    {:noreply, socket}
  end

  @impl true
  def handle_info(:select_category, socket) do
    # TODO: Handle this in the question.ex liveview and do not reassign board control.
    {:noreply, socket
    |> push_redirect(to: Routes.play_path(socket, DemogameWeb.JeopardyLive.Play, socket.assigns.current_player.id))}
  end

  @impl true
  def handle_info({:question_response, _correct}, %{assigns: %{state: :waiting}} = socket) do
    {:noreply, socket |> assign(state: :ready)}
  end

  @impl true
  def handle_info({:player_buzzed_in, player}, socket) do
    if player.id != socket.assigns.current_player.id do
      {:noreply, socket |> assign(state: :waiting)}
    else
      {:noreply, socket}
    end
  end

  @impl true
  def handle_info({:buzzer_timeout, player}, socket) do
    if player.id != socket.assigns.current_player.id do
      {:noreply, socket |> assign(state: :ready)}
    else
      {:noreply, socket}
    end
  end

  @impl true
  def handle_info(:no_answer, %{assigns: %{state: :buzzed_in}} = socket) do
    broadcast(socket.assigns.game.id, {:buzzer_timeout, socket.assigns.current_player})
    {:noreply, socket |> assign(state: :locked_out)}
  end

  @impl true
  def handle_info(:no_answer, socket) do
    {:noreply, socket}
  end

  @impl true
  def handle_info(_msg, socket) do
    {:noreply, socket}
  end

  defp broadcast(game_id, msg) do
    Phoenix.PubSub.broadcast(Demogame.PubSub, "game:#{game_id}", msg)
  end
end
