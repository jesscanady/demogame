defmodule Demogame.Repo.Migrations.CreateGames do
  use Ecto.Migration

  def change do
    create table(:games) do
      add :code, :string
      add :type, :string
      add :status, :string, default: "waiting_for_players"

      timestamps()
    end
  end
end
