defmodule Demogame.Repo.Migrations.CreateQuestions do
  use Ecto.Migration

  def change do
    create table(:questions) do
      add :category_id, :integer
      add :points, :integer
      add :text, :string

      timestamps()
    end
  end
end
