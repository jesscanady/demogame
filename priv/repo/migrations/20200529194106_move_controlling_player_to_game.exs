defmodule Demogame.Repo.Migrations.MoveControllingPlayerToGame do
  use Ecto.Migration

  def change do
    alter table(:players) do
      remove :controlling
    end

    alter table(:games) do
      add :controlling_player_id, references(:players)
    end
  end
end
