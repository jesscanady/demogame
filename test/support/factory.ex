defmodule Demogame.Factory do
   use ExMachina.Ecto, repo: Demogame.Repo

   @spec game_factory :: Demogame.Game.t()
   def game_factory do
    %Demogame.Game{
      code: "AAAA",
      status: "waiting_for_players"
    }
   end

   def running_game_factory do
     struct!(
       game_factory(),
       %{
         status: "running"
       }
     )
   end

   def player_factory do
     %Demogame.Player{
       name: sequence(:name, &"Player #{&1}"),
       game: build(:game)
     }
   end

   def question_factory do
     %Demogame.Question{
       category_id: 1,
       points: 100,
       text: "Something witty."
     }
   end
end
