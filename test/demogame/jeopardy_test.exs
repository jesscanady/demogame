defmodule Demogame.JeopardyTest do
  use Demogame.DataCase

  alias Demogame.Jeopardy

  test "updates game status" do
    game = insert(:game, status: "waiting_for_players")
    game = Jeopardy.update_game_status(game, "running")
    assert game.status == "running"
  end

  describe "add_player_to_game/2" do
    test "can join an in progress game as an existing player" do
      game = insert(:game, status: "running")
      player = insert(:player, game: game)

      assert {:rejoin, player} = Jeopardy.add_player_to_game(game, player.name)
    end

    test "can join a game that is waiting for players" do
      game = insert(:game, status: "waiting_for_players")
      player = insert(:player, game: game)

      assert {:ok, player} = Jeopardy.add_player_to_game(game, player.name)
    end
  end

  test "set_controlling_player/1 sets the player" do
    game = insert(:game)
    player = insert(:player, game: game)

    Jeopardy.set_controlling_player(player)

    player = Jeopardy.find_player(player.id)
    game   = Jeopardy.find_game(game.id)
    assert game.controlling_player_id == player.id
  end

  describe "get_or_assign_controlling_player/1" do
    test "returns the currently controlling player if the game has one" do
      player = insert(:player)
      game = insert(:game, controlling_player: player)

      assert(Jeopardy.get_or_assign_controlling_player(game) == player)
    end

    test "assign and return the currently controlling player if the game doesn't have one" do
      game   = insert(:game, controlling_player_id: nil)
      player = insert(:player, game: game)

      assert(Jeopardy.get_or_assign_controlling_player(game).id == player.id)
    end
  end

  describe "get_controlling_player/1" do
    test "gets the player who controls the board" do
      game = insert(:game)
      controlling_player = insert(:player,
                                  game: game)
      insert(:player,
            game: game)

      Jeopardy.set_controlling_player(controlling_player)
      game = Jeopardy.find_game(game.id)

      assert Jeopardy.get_controlling_player(game).id == controlling_player.id
    end

    test "returns nil if no player controls the board" do
      game = insert(:game)

      assert is_nil(Jeopardy.get_controlling_player(game)) 
    end
  end

  describe "find_game/1" do
    test "can find a game that has no players" do
      game = insert(:game)

      assert Jeopardy.find_game(game.id).code == game.code
    end
  end

  describe "find_question/2" do
    test "finds a question" do
      question = insert(:question, category_id: 1, points: 100, text: "The clue.")
      assert Jeopardy.find_question(1, 100).text == question.text
    end
  end

  describe "select_question_for_game/2" do
    test "stores the selected question on the game" do
      game     = insert(:game)
      question = %{id: 1}
      
      Jeopardy.select_question_for_game(game, question)

      game = Jeopardy.find_game(game.id)

      assert(game.current_question_id == question.id)
    end
  end
end

