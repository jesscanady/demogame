defmodule DemogameWeb.AnswerLiveTest do
  use DemogameWeb.ConnCase

  import Phoenix.LiveViewTest

  setup %{conn: conn} do
    player      = insert(:player)
    player2     = insert(:player, game: player.game)
    question_id = 1
    {:ok, answer_view, _disconnected_html} = live(conn, "/answer/#{question_id}/#{player.id}")
    {:ok, answer_view_2, _disconnected_html} = live(conn, "/answer/#{question_id}/#{player2.id}")

    {:ok, %{player: player, question_id: question_id, answer_view: answer_view, answer_view_2: answer_view_2, conn: conn}}
  end

  test "when nobody has buzzed in", %{answer_view: answer_view} do
    assert answer_view
    |> element("#buzzer")
    |> has_element?()
  end

  test "buzzing in", %{answer_view: answer_view} do
    answer_view
    |> element("#buzzer")
    |> render_click()

    assert answer_view
    |> element("#answers")
    |> has_element?()
  end

  test "can't buzz in after you've answered the question wrong", %{answer_view: answer_view} do
    answer_view
    |> element("#buzzer")
    |> render_click()

    assert answer_view
    |> element("#answer_2")
    |> render_click() =~ "TRY AGAIN"
  end

  test "when a player buzzes in, other players have to wait", %{answer_view: answer_view, answer_view_2: answer_view_2} do
    answer_view
    |> element("#buzzer")
    |> render_click()

    assert answer_view_2
    |> render() =~ "PLEASE WAIT"
  end

  test "when the buzzed in player answers incorrectly, other players may buzz in", %{answer_view: answer_view, answer_view_2: answer_view_2} do
    answer_view
    |> element("#buzzer")
    |> render_click()

    answer_view
    |> element("#answer_2")
    |> render_click()

    assert answer_view_2
    |> render() =~ "BUZZ IN"
  end
end
