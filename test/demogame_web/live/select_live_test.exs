defmodule DemogameWeb.SelectLiveTest do
  use DemogameWeb.ConnCase

  import Phoenix.LiveViewTest

  alias Demogame.Jeopardy

  test "remembers the selected question", %{conn: conn} do
    game   = insert(:game)
    player = insert(:player, game: game)
    {:ok, select_view, _disconnected_html} = live(conn, "/select/#{player.id}")

    select_view
    |> element("#category-1")
    |> render_click()

    select_view
    |> element("#amount-100")
    |> render_click()

    game = Jeopardy.find_game(game.id)
    assert game.current_question_id == 1

    assert_redirect(select_view, "/answer/#{game.current_question_id}/#{player.id}")
  end

  test "redirects to the answer view if a question is already selected", %{conn: conn} do
    question = %{id: 1}
    game = insert(:game, current_question_id: question.id)
    player = insert(:player, game: game)

    {:error, {:live_redirect, %{to: to}}} = live(conn, "/select/#{player.id}")
    assert(to == "/answer/#{question.id}/#{player.id}")
  end
end
