defmodule DemogameWeb.QuestionLiveTest do
  use DemogameWeb.ConnCase

  import Phoenix.LiveViewTest

  test "shows the player that buzzed in", %{conn: conn} do
    player = insert(:player)
    {:ok, question_view, _disconnected_html} = live(conn, "/question/#{player.game.id}/1")
    send(question_view.pid, {:buzz, player})

    assert question_view
    |> render() =~ player.name
  end
end
